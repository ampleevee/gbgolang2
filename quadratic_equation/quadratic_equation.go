package quadratic_equation

import (
	"fmt"
	"math/cmplx"
)

func PrintCount(A, B, C float64) {

	fmt.Printf("Уровнение %.2fx^2 + %.2fx + %.2f = 0\n", A, B, C)

	var a, b, c, sqrtD, x1, x2 complex128

	a = complex(A, 0)
	b = complex(B, 0)
	c = complex(C, 0)

	sqrtD = cmplx.Sqrt((b * b) - 4*a*c)

	x1 = (-b + sqrtD) / 2 * a
	x2 = (-b - sqrtD) / 2 * a

	fmt.Printf("sqrtD = %.2f\n", sqrtD)
	fmt.Printf("x1 = %.2f\n", x1)
	fmt.Printf("x2 = %.2f\n", x2)

}
