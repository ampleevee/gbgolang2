package looptester

import "fmt"

func LoopTester3() {
	s := "Hello, world! моя семья 世界"
	for i, ch := range s {
		fmt.Printf("%d = %c\n", i, ch)
	}
}

func LoopTester2(a uint) {

	for a = 1; a <= 10; a++ {

		if (a == 3) {

			continue

		}

		fmt.Println(a)

	}

	fmt.Printf("final a=%v\n", a)
}

func LoopTester1(a uint) {

	for a = 1; a <= 10; a++ {

		fmt.Println(a)

		if (a == 3) {
			break
		}

	}

	fmt.Printf("final a=%v\n", a)
}

func LoopTester(a uint) {

	for a = 1; a <= 10; a++ {

		b := 1 << a

		fmt.Printf("1<<%d=%d (%08b)\n", a, b, b)

	}

	fmt.Printf("final a=%v\n", a)
}
