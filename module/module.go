package module

func CountModule(a int) int {
	if a < 0 {
		a = -a
	}
	return a
}

func SignAbs(a int) (int, int) {

	if a < 0 {

		return -1, -a

	} else if a > 0 {

		return 1, a

	}

	return 0, 0

}
