package testslices

import "fmt"

func Average(s []int) int {
	defer fmt.Println("1 defer 0")
	result := 0
	for _, v := range s {
		result += v
	}
	return result / len(s)
}

func Average1(s ...int) int {
	defer fmt.Println("1 defer 1")
	result := 0
	for _, v := range s {
		result += v
	}
	return result / len(s)
}

func Test1() {
	defer fmt.Println("1 defer -1")
	total := 0
	s := []int{98, 94, 11, 0, 23}

	for i, v := range s {
		println(i, v)
		total += v
	}

	fmt.Println(total / len(s))

}
